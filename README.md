# HTML Canvas Slider
A super simple image slider built on the HTML5 canvas.

## Usage

Include the slider.min.js file: 

    <head>
    	...
    	<script src="slider.js"></script>
    </head>

In your HTML, add a canvas element with a `data-slider` attribute, and give the slider a name: 
    
    <canvas data-slider="alpha"></canvas>

In your javascript, call the slider function and add some parameters: 

    slider(
		{
			element: "alpha",
			size: [640, 200],
			images: ["img/image1.jpg", "img/image2.jpg", "img/image3.jpg"],
			secondsPerImage: 3
		}
	);

## Parameters

* `element` (string) refers to the data-slider value of the canvas element

* `size` (array[int]) the width and height of the slider

* `images` (array[string]) an array of URLs to each image

* `secondsPerImage` (int) the time, in seconds, to wait before moving to the next image