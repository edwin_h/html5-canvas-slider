var slider = function(params){

	var params = {
		element: params.element || null,
		size: params.size || [640, 200],
		images: params.images || [],
		secondsPerImage: params.secondsPerImage || 3
	}

	var images = [],
		canvas,
		ctx;

	canvas = document.querySelectorAll("canvas[data-slider='"+params.element+"']")[0];
	ctx = canvas.getContext("2d");

	canvas.width = params.size[0];
	canvas.height = params.size[1];

	for(var k in params.images){
		images[k] = new Image();
		images[k].onload = function(){
			start();
		};
		images[k].src = params.images[k];
	}

	var start = function(){
		drawImage(0);
		var currentImage = 1;
		setInterval(function(){
			drawImage(currentImage);
			currentImage++;
			if(currentImage >= images.length){
				currentImage = 0;
			}
		}, (params.secondsPerImage * 1000));
	}
	
	var drawImage = function(which){
		var which = which || 0;
		ctx.drawImage(images[which], 0, 0, canvas.width, canvas.height);
	}
}